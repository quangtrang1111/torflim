var blocks = require('blocks');

// enables server-side rendering
blocks.server({
  port: 3000,
  static: 'app',
  cache: false,
  use: null
});