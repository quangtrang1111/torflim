function Home() {
  this.options = {
    route: '/',
    url: '/home/index.html'
  }
  
  this.top = blocks.observable([])
  this.update = blocks.observable([])
  this.hot = blocks.observable([])
  this.isLoading = isLoading
  
  var self = this
  
  this.ready = function () {
    isLoading(true)
    
      getData('https://yts.ag/api/v2/list_movies.json?sort_by=download_count&limit=5', function (data) {
    let response = JSON.parse(data)
    var movies = response.data.movies.map(function (item, index) {
      item.active = (index === 0) ? 'active' : ''
      return item
    })
    self.top(response.data ? movies : [])
    
      getData('https://yts.ag/api/v2/list_movies.json?sort_by=date_added&limit=20', function (data) {
    let response = JSON.parse(data)
    self.update(response.data ? response.data.movies : [])
    
      getData('https://yts.ag/api/v2/list_movies.json?sort_by=download_count&limit=20', function (data) {
    let response = JSON.parse(data)
    self.hot(response.data ? response.data.movies : [])
    isLoading(false)
    
          	getData('../res/menu.json', function (data) {
              $('#treeview').treeview({
          color: "#428bca",
          expandIcon: 'glyphicon glyphicon-chevron-right',
          collapseIcon: 'glyphicon glyphicon-chevron-down',
          nodeIcon: 'glyphicon glyphicon-bookmark',
		  enableLinks: true,
          data: JSON.parse(data)
        })
   	})
    
  })
  })
  })
  }
  
  this.routed = function () {
    var video = document.getElementById('video-player')
    if (video) {
      video.pause()
      isPlaying(false)
    }
  }  
  

}