function Play() {
  this.options = {
    route: '/play/{{movieId}}',
    url: '/play/index.html'
  }

  this.playerClass = blocks.observable(function () {
    return isPlaying() ? 'col-md-12' : 'col-md-10'
  }, this)

  this.suggestions = blocks.observable([])
  this.isLoading = isLoading
  this.domain = window.location.origin
  this.movieId
  
  // this.clickMovie = function (event, id) {
  //   this.route('play', id)
  // }

  this.jumpFlim = function (event) {
    event.preventDefault()
	this.route('404')
  }
	
  this.routed = function (params) {      
  this.movieId = params.movieId
  
    var video = document.getElementById('video-player')
    if (video) {
      video.pause()
      isPlaying(false)
    } 
  
    let self = this
    isLoading(true)
	
    // var client = new WebTorrent()
    // var torrentId = 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d&dn=sintel.mp4&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&tr=wss%3A%2F%2Ftracker.webtorrent.io&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel-1024-surround.mp4'

    // client.add(torrentId, function (torrent) {
    //   var file = torrent.files[0]
    //   console.log(file)
    //   file.renderTo('#video-player')
    // })
	
    getData('https://yts.ag/api/v2/movie_details.json?movie_id=' + params.movieId, function (data) {      
      if (data) {
		var response = JSON.parse(data)
		if(response.data.movie.id == 0) {
			isLoading(false)
			self.route('404')
			return
		}
			
        playingMovie(response.data.movie)
        
        // get suggestions movie
        getData('https://yts.ag/api/v2/movie_suggestions.json?movie_id=' + params.movieId, function (data) {
          var response = JSON.parse(data)
          self.suggestions(response.data ? response.data.movies : [])
          isLoading(false)
        })
      } else {
        isLoading(false)
        self.route('404')
		return
      }
    })

    loadFB(document, 'script', 'facebook-jssdk')
  }
}
