// TODO: try to change model
function Detail() {
  this.options = {
    url: '/play/player/detail/index.html'
  }

  this.movie = playingMovie

  this.genre = blocks.observable(function () {
    if (this.movie().genres) {
      var genres = this.movie().genres
      var genre = genres[0]
      for (var index = 1; index < genres.length; index++) {
        genre = genre + ', ' + genres[index]
      }

      return genre
    }
  }, this)
  
  this.torrent = blocks.observable(function () {
    if (this.movie().torrents) {
      var url
      this.movie().torrents.forEach(function (item) {      
        if (item.quality == '1080p') {
          url = item.url
        }
      })
      
      return url
    }
  }, this)
}