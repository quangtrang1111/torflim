function Search() {
  this.options = {
    route: '/search/{{key}}',
    url: '/search/index.html'
  }

  this.movies = blocks.observable([])
  this.hot = blocks.observable([])
  this.isFounded = blocks.observable(false)
  this.isLoading = isLoading

  this.ready = function () {
          getData('../res/menu.json', function (data) {
              $('#treeview-search').treeview({
          color: "#428bca",
          expandIcon: 'glyphicon glyphicon-chevron-right',
          collapseIcon: 'glyphicon glyphicon-chevron-down',
          nodeIcon: 'glyphicon glyphicon-bookmark',
		  enableLinks: true,
          data: JSON.parse(data)
        })
   	})
  }
  
  this.routed = function (params) {
        var video = document.getElementById('video-player')
    if (video) {
      video.pause()
      isPlaying(false)
    }
    
          var self = this
          isLoading(true)

          if (params.key == 'filter=action') {
            getData('https://yts.ag/api/v2/list_movies.json?sort_by=year&genre=action&limit=20', function (data) {
                  let response = JSON.parse(data)
                  self.movies(response.data ? response.data.movies : [])
                  self.isFounded(true)
                  isLoading(false)
            })
          } else {
            getData('https://yts.ag/api/v2/list_movies.json?query_term=' + params.key, function (data) {
              var response = JSON.parse(data)
              if (response.data.movies) {
                self.isFounded(true)
                self.movies(response.data.movies)
                isLoading(false)
              } else {
              self.movies([])
              self.isFounded(false)
              
                  getData('https://yts.ag/api/v2/list_movies.json?sort_by=download_count&limit=20', function (data) {
                  let response = JSON.parse(data)
                  self.hot(response.data ? response.data.movies : [])
                  isLoading(false)
                })
              }        
            })
            
          }
  }
}
