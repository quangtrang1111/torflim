function NavBar() {
  this.movies = blocks.observable([])

  this.options = {
    url: '/navbar/index.html'
  }

  this.filterValue = blocks.observable()
  this.isLogin = blocks.observable(false)
  this.isFinding = blocks.observable()

  // this.items = blocks.observable(movies)
  //   .extend('filter', function (movie) {
  //     return this.filterValue() && movie.title.toLowerCase().indexOf(this.filterValue().toLowerCase()) > -1
  //   })
  //   .extend('sort', function (a, b) {
  //     return b.year - a.year
  //   })
  //   .extend('take', 5)

  this.ready = function () {
	var loginTime = localStorage.getItem('torFlimLoginTime')
	if (loginTime) {
	  this.isLogin((new Date()).getTime() - localStorage.getItem('torFlimLoginTime') < 1800000) //30 minutes
	}
  }
  
  this.searchMovie = function (event) {
    event.preventDefault()

    if (this.filterValue()) {  
      this.movies.reset()
      this.route('search', this.filterValue())
    }
  }

  this.selectMovie = function (event, movieId) {
    this.movies.reset()
    this.route('play', movieId)
  }

  this.findingMovie = function (event) {
    var key = event.target.value
    this.filterValue(key)

    if (key) {
      this.isFinding('isFinding')
      var self = this

      getData('https://yts.ag/api/v2/list_movies.json?limit=5&sort_by=year&order_by=desc&query_term=' + key, function (data) {
        var response = JSON.parse(data)
        self.movies(response.data.movies ? response.data.movies : [])
        self.isFinding('')
      })
    } else {
      this.movies([])
    }
  }
  
  this.login = function (event) {
    event.preventDefault()
	  
    localStorage.setItem('torFlimLoginTime', (new Date()).getTime())
    this.isLogin(true)
    $('#exampleModal').modal('hide')
  }
  
  this.logout = function () {
    localStorage.removeItem('torFlimLoginTime')
    this.isLogin(false)
  }
}
