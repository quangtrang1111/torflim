function Error() {
  this.options = {
    route: '/404',
    url: '/error/index.html'
  }  
    
  this.routed = function () {
    var video = document.getElementById('video-player')
    if (video) {
      video.pause()
      isPlaying(false)
    }
  }  
  
}

function Lost() {
  this.options = {
    route: '/*'
  }
  
  this.routed = function () {
	  this.route('404')
  }
}