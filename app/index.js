var App = blocks.Application({
	history: 'pushState'
})

var isPlaying = blocks.observable(false)
var playingMovie = blocks.observable({})
var isLoading = blocks.observable(false)

App.View('Loading', new Loading())
App.View('NavBar', new NavBar())
App.View('Home', new Home())
App.View('Error', new Error())
App.View('Play', new Play())
App.View('Play', 'Player', new Player())
App.View('Play', 'Detail', new Detail())
App.View('Search', new Search())
App.View('Lost', new Lost())
App.View('Filters', new Filters())